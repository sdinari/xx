create or replace PACKAGE BODY xxsgma_ap_wflot_pkg AS
/* $Header:  v1.0 SDINARI : ATOS 2022 (MOROCCO) $ */

procedure xx_log(p_msg varchar2) is
begin
   xx_log_msg('#pkg#xxsgma_ap_wflot_pkg# '||p_msg);
end;

--
--
    PROCEDURE process_doc_approval (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    l_lot_header_id number;
    l_role   varchar2(240);
    BEGIN
       
        l_lot_header_id := WF_ENGINE.GetItemAttrNumber(itemtype,   itemkey,   'LOT_HEADER_ID');
        l_role := WF_ENGINE.GetItemAttrText(itemtype,   itemkey,   'APPROVER_NAME');
        --
           AME_API2.updateApprovalStatus2(applicationIdIn => 200,
             transactionIdIn     => to_char(l_lot_header_id),
             approvalStatusIn    => AME_UTIL.approvedStatus,
             approverNameIn  => l_role,
             transactionTypeIn =>  'APINVLOT',
             itemClassIn     => ame_util.headerItemClassName,
             itemIdIn        => to_char(l_lot_header_id));
        ---
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
                     exception
                     when others then xx_log('process_doc_approval :'||sqlerrm);
    END process_doc_approval;
 PROCEDURE set_document_approver (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    BEGIN
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    EXCEPTION
        WHEN OTHERS THEN
            wf_core.context('APLOTAPR', 'set_document_approver', itemtype, itemkey, to_char(actid)
                          , funcmode);

            RAISE;
    END set_document_approver;

    PROCEDURE notification_handler (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS

        l_invoice_id            NUMBER(15);
        l_iteration             NUMBER;
        l_status                VARCHAR2(50);
        l_response              VARCHAR2(50);
        l_nid                   NUMBER;
        l_forward_to_person_id  NUMBER;
        l_result                VARCHAR2(100);
        l_orig_system           wf_roles.orig_system%TYPE;
        l_orig_sys_id           wf_roles.orig_system_id%TYPE;
        l_role                  VARCHAR2(50);
        l_role_display          VARCHAR2(150);
        l_org_id                NUMBER(15);
        l_name                  wf_users.name%TYPE; --bug 8620671
                                                                l_display_name          VARCHAR2(150);
        l_forward_to_user_id    wf_roles.orig_system_id%TYPE;
        l_esc_approver          ame_util.approverrecord;
        l_rec_role              VARCHAR2(50);
        l_comments              ap_inv_aprvl_hist.approver_comments%TYPE; --Bug16295758
                                                                l_hist_id               NUMBER(15);
        l_amount                ap_invoices_all.invoice_amount%TYPE;
        l_user_id               NUMBER(15);
        l_login_id              NUMBER(15);
        l_hist_rec              ap_inv_aprvl_hist%rowtype;
        l_notf_iteration        NUMBER;
        l_role_name             VARCHAR2(50);
        l_esc_flag              VARCHAR2(1);
        l_esc_role              VARCHAR2(50);
        l_esc_role_actual       VARCHAR2(50);
        l_role_actual           VARCHAR2(50);
        l_fwd_role              VARCHAR2(50);
        l_invoice_total         NUMBER;
        l_approver       varchar2(60);
        CURSOR c_get_user (
            p_rec_role IN VARCHAR2
        ) IS
        SELECT
            user_id
          , employee_id
        FROM
            fnd_user
        WHERE
            user_name = p_rec_role;

        CURSOR c_get_response (
            p_invoice_id IN NUMBER
        ) IS
        SELECT
            response
        FROM
            ap_inv_aprvl_hist
        WHERE
            approval_history_id = (
                SELECT
                    MAX(approval_history_id)
                FROM
                    ap_inv_aprvl_hist
                WHERE
                    invoice_id = p_invoice_id
            );

    BEGIN
 --Invoice Lot 
 --&#HDR_LOT_NUMBER (&#HDR_TOTAL_LOT_FORMATTED) 
 --requires your approval
        l_approver :=wf_engine.getitemAttrText(itemtype,itemkey,'APPROVER');
        xx_log('notification_handler :: notification_handler l_approver='||l_approver);
        -- l_role :=l_approver;
        -- l_display_name := l_role;
        --wf_engine.setitemattrtext(itemtype, itemkey                                , 'APPROVER'                                , l_role);
        --wf_engine.setitemattrtext(itemtype, itemkey                                , 'APPROVER_NAME'                                , l_display_name);
        --wf_engine.setitemattrtext(itemtype, itemkey, 'LOT_NUMBER', '10544');
        --wf_engine.setitemattrtext(itemtype, itemkey, 'TOTAL_FORMATTED', '1777.88 MAD');
/*
      resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';*/
    EXCEPTION
        WHEN OTHERS THEN
            xx_log('notification_handler :: '||SQLERRM);
            wf_core.context('AP_WF', itemtype, itemkey, to_char(actid), funcmode);
            RAISE;
    END notification_handler;
    
   --
   --
    PROCEDURE reject_handler (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
 
        l_approver       VARCHAR2(60);
    BEGIN
    xx_log('reject_handler <Start>');
        l_approver := wf_engine.getitemattrtext(itemtype, itemkey, 'APPROVER');
        xx_log('reject_handler :: notification_handler l_approver=' || l_approver);
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    EXCEPTION
        WHEN OTHERS THEN
            xx_log('reject_handler :: ' || sqlerrm);
            wf_core.context('AP_WF', itemtype, itemkey, to_char(actid), funcmode);
            RAISE;
    END reject_handler;
   
   --
   --
    

    PROCEDURE process_doc_rejection (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    BEGIN
        NULL;
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    END;

    PROCEDURE forward_check (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    BEGIN
        NULL;
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    END;

    PROCEDURE escalate_doc_approval (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    BEGIN
        NULL;
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    END;

    PROCEDURE process_sub_approval_response (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) IS
    BEGIN
        NULL;
        resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    END;

    PROCEDURE show_lot_detail (
        document_id    IN  VARCHAR2
      , display_type   IN  VARCHAR2
      , document       IN OUT NOCOPY VARCHAR2
      , document_type  IN OUT NOCOPY VARCHAR2
    ) IS
          v_step varchar2(30);
           l_invoice_url varchar2(512);
           l_ITEMKEY varchar2(512);
           l_notificationId varchar2(512);
           l_lot_header_id number;
           l_mnt_lot_mad number;
           l_nbr_factures_lot number;
        CURSOR cur_invoice(p_lot_header_id number) IS
        SELECT 
            apinv.invoice_id
          , apinv.remit_to_supplier_name    num_fournisseur
          , apinv.invoice_num               num_facture
          , apinv.description               desc_facture
          , apinv.invoice_amount            mnt_ttc_mad
          , apinv.invoice_currency_code     devise
          , apinv.invoice_date              date_facture
          , apinv.payment_method_code       classe_reglement
          , '0'                             commande
          , 'http://www.google.fr '         lien_facture
        FROM
            ap_invoices_all apinv
            ,xxsgma_invlot_lines xx
        WHERE
            apinv.invoice_id = xx.invoice_id
            and xx.lot_header_id = p_lot_header_id
            order by xx.LOT_LINE_ID desc
            ;
    --
    cursor cur_lot_header(p_lot_header_id number) is
    select lot_header_id,NUM_LOT,DATE_LOT,(select user_name from fnd_user fu where fu.user_id= loth.user_id) user_name
    from 
    XXSGMA_INVLOT_HEADERS loth
    where loth.lot_header_id=p_lot_header_id;
 
    BEGIN
        v_step :='X0';
        xx_log('#show_lot_detail#');
        xx_log('document_id='||document_id);
        l_lot_header_id :=replace(document_id,'LOT_HEADER_ID=','');
        get_lot_info(p_lot_header_id => l_lot_header_id, p_nombre_factures => l_nbr_factures_lot, p_total_factures => l_mnt_lot_mad);
        document := '
        <h2>Information Lot</h2>        
        <table>
            <tr>
              <th class="OraTableColumnHeader">Numéro du lot</th>
              <th class="OraTableColumnHeader">Nom du lot</th>
              <th class="OraTableColumnHeader">Date du lot</th>
              <th class="OraTableColumnHeader">Montant du lot en MAD</th>
              <th class="OraTableColumnHeader">Emetteur</th>
            </tr>';
    v_step :='X1';
    for r1 in cur_lot_header(l_lot_header_id) loop
    document :=document ||'
    <tr>
      <td class="OraTableCellText OraTableBorder1100">'||r1.lot_header_id||'</td>
      <td class="OraTableCellText OraTableBorder1100">'||r1.num_lot||'</td>
      <td class="OraTableCellText OraTableBorder1100">'||r1.date_lot||'</td>
      <td class="OraTableCellText OraTableBorder1100">'||to_char(l_mnt_lot_mad,'999G999G999G990D00')||'</td>
      <td class="OraTableCellText OraTableBorder1100">'||r1.user_name||'</td>
    </tr>';
  end loop;
   v_step :='X2';
    document :=document ||'</table>
<h2>Liste des factures</h2>   
<table>
  <tr>
    <th class="OraTableColumnHeader">Num. furnisseur</th>
    <th class="OraTableColumnHeader">Num. facture</th>
    <th class="OraTableColumnHeader">Description facture</th>
    <th class="OraTableColumnHeader">Montant TTC (MAD)</th>
    <th class="OraTableColumnHeader">Devise</th>
    <th class="OraTableColumnHeader">Date facture</th>
    <th class="OraTableColumnHeader">Classe de règlement</th>
    <th class="OraTableColumnHeader">Commande</th>
    <th class="OraTableColumnHeader">Voir Facture</th>
  </tr>';
        v_step :='X3';
        FOR r0 IN cur_invoice(l_lot_header_id) LOOP
          
          
           l_ITEMKEY:='1';
           l_notificationId:='1';
           
             
           
           l_invoice_url := '/OA_HTML/OA.jsp?OAFunc=AP_INV_LINE_DETAILS&INVOICEID='||r0.INVOICE_ID||'&NOTIFICATIONCONTEXT=APPROVALDOCUMENT&ISINTERNAL=Y&ITEMKEY=1&notificationId=7955170&PINSTANCELABEL=DOCUMENT_APPROVAL_REQUEST';
            document := document|| '
  <tr>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.num_fournisseur                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.num_facture||'<a href='||l_invoice_url||' target="_top" class="xh">link</a>'                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.desc_facture                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.mnt_ttc_mad                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.devise                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.date_facture                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.classe_reglement                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.commande                        || '</td>
  <td class="OraTableCellText OraTableBorder1100">'                        || r0.lien_facture                        || '</td>
  </tr>
  ';
        END LOOP;
v_step :='X4';
        document := document || '</table>';
    v_step :='X9';
    exception when others then 
    xx_log('show_lot_detail :'||SQLERRM);
     xx_log('show_lot_detail v_step :'||v_step);
      xx_log('show_lot_detail document_id :'||document_id);
    raise;
    END show_lot_detail;

--
--
PROCEDURE get_lot_info (
    p_lot_header_id    IN      NUMBER
  , p_nombre_factures  IN OUT  NUMBER
  , p_total_factures   IN OUT  NUMBER
) IS BEGIN
    SELECT
        COUNT(1)
      , SUM(nvl(apinv.exchange_rate, 1) * apinv.invoice_amount)
    INTO
        p_nombre_factures
    , p_total_factures
    FROM
        ap_invoices_all apinv
    WHERE
        apinv.invoice_id IN (
            SELECT
                xx.invoice_id
            FROM
                xxsgma_invlot_lines xx
            WHERE
                xx.lot_header_id = p_lot_header_id
        );

EXCEPTION
    WHEN OTHERS THEN
        NULL; end;
--
--
PROCEDURE start_lot_approval(P_LOT_HEADER_ID number)
is
    v_itemtype  VARCHAR2(50);
    v_itemkey   VARCHAR2(50);
    v_process   VARCHAR2(50);
    v_userkey   VARCHAR2(50);
    v_owner      VARCHAR2(50);
    v_total_number number;
    v_total_mnt number;
    v_step varchar2(30);
    v_NUM_LOT varchar2(60);
begin
v_step := 'X0';

select user_name into  v_owner  from fnd_user where user_id=fnd_global.user_id;
     xx_log_msg('P_LOT_HEADER_ID='||P_LOT_HEADER_ID);
     ame_api2.clearallApprovals(200,'APINVLOT',P_LOT_HEADER_ID);
     xx_log_msg('v_owner='||v_owner);
    v_itemtype := 'APLOTAPR';
    v_itemkey := P_LOT_HEADER_ID+1000*xx_seq.nextval();
    v_userkey := 'LOT_'||v_itemkey;
    v_process := 'APPROVAL_LOT';
    wf_engine.threshold :=1.5 ;--- -1;
    wf_engine.createprocess(v_itemtype, v_itemkey, v_process);
    wf_engine.setitemuserkey(v_itemtype, v_itemkey, v_userkey);
    wf_engine.setitemowner(v_itemtype, v_itemkey, v_owner);
    --
    select NUM_LOT into v_NUM_LOT
    from XXSGMA_INVLOT_HEADERS where lot_header_id=p_lot_header_id;
     wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'LOT_NUMBER', v_NUM_LOT);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'TOTAL_FORMATTED', '1777.88 MAD');
    --
--
v_step := 'X1';

get_lot_info(p_lot_header_id => p_lot_header_id, p_nombre_factures => v_total_number, p_total_factures => v_total_mnt);

--    
    v_step := 'X2';
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'LOT_HEADER_ID',    P_LOT_HEADER_ID);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'DOCUMENT_APPROVER',   v_owner);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, '#HDR_LOT_NUMBER', v_total_number);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'TOTAL_FORMATTED', v_total_mnt||' MAD');
    
    wf_engine.startprocess(v_itemtype, v_itemkey);
    v_step := 'X9';
    COMMIT;
exception when others
  then xx_log('#EXP# xxsgma_ap_wflot_pkg.start_lot_approval :'||SQLERRM);
         xx_log('v_step='||v_step);
end;



--new
PROCEDURE start_lot_approval2(P_LOT_HEADER_ID number)
is
    v_itemtype  VARCHAR2(50);
    v_itemkey   VARCHAR2(50);
    v_process   VARCHAR2(50);
    v_userkey   VARCHAR2(50);
    v_owner      VARCHAR2(50);
    v_total_number number;
    v_total_mnt number;
    v_step varchar2(30);
    v_NUM_LOT varchar2(60);
begin
v_step := 'X0';

select user_name into  v_owner  from fnd_user where user_id=fnd_global.user_id;
     xx_log_msg('P_LOT_HEADER_ID='||P_LOT_HEADER_ID);
     ame_api2.clearallApprovals(200,'APINVLOT',P_LOT_HEADER_ID);
     xx_log_msg('v_owner='||v_owner);
    v_itemtype := 'APLOTAPR';
    v_itemkey := P_LOT_HEADER_ID+1000*xx_seq.nextval();
    v_userkey := 'LOT_'||v_itemkey;
    v_process := 'APLOTAPR_MASTER';
    wf_engine.threshold :=1.5 ;--- -1;
    wf_engine.createprocess(v_itemtype, v_itemkey, v_process);
    wf_engine.setitemuserkey(v_itemtype, v_itemkey, v_userkey);
    wf_engine.setitemowner(v_itemtype, v_itemkey, v_owner);
    --
    select NUM_LOT into v_NUM_LOT
    from XXSGMA_INVLOT_HEADERS where lot_header_id=p_lot_header_id;

    --
--
v_step := 'X1';

get_lot_info(p_lot_header_id => p_lot_header_id, p_nombre_factures => v_total_number, p_total_factures => v_total_mnt);

--    
    v_step := 'X2';
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'LOT_HEADER_ID',    P_LOT_HEADER_ID);
    --wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'DOCUMENT_APPROVER',   v_owner);
    --wf_engine.setitemattrtext(v_itemtype, v_itemkey, '#HDR_LOT_NUMBER', v_total_number);
    --wf_engine.setitemattrnumber(v_itemtype, v_itemkey, 'LOT_HEADER_ID', p_lot_header_id);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'LOT_NUMBER', v_NUM_LOT);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'TOTAL_LOT', v_total_mnt);
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'TOTAL_FORMATTED', v_total_mnt||' MAD');
    wf_engine.setitemattrtext(v_itemtype, v_itemkey, 'MASTER_ITEM_KEY', v_itemkey);
    --
    
    wf_engine.startprocess(v_itemtype, v_itemkey);
    v_step := 'X9';
    COMMIT;
exception when others
  then xx_log('#EXP# ::::: start_lot_approval2 :'||SQLERRM);
         xx_log('v_step='||v_step);
end;
    PROCEDURE is_approval_complete_old (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) is 
    l_approvalProcessCompleteYNOut varchar2(1);
     l_tab_approvers ame_util.approversTable2;
     v_step varchar2(30);
     l_next_approver ame_util.approverRecord;
     v_child_itemkey varchar2(60);
     v_child_approver  varchar2(60);
     v_child_userkey varchar2(60);
    begin
    v_step :='X0';
     
  
        AME_API2.getNextApprovers4(applicationIdIn =>200,
                              transactionTypeIn =>'APINVLOT',
                              transactionIdIn => '27',
                              flagApproversAsNotifiedIn => ame_util.booleanFalse,
                              approvalProcessCompleteYNOut => l_approvalProcessCompleteYNOut,
                              nextApproversOut =>l_tab_approvers);
    v_step :='X1';
         -- xx_log('-->approver name :'||l_next_approver.first_name||', '||l_next_approver.last_name);
           xx_log('-->approver name :'||l_tab_approvers(1).name);     
           if (l_approvalProcessCompleteYNOut = ame_util.booleanTrue) then
           xx_log('Process complete =True');
           resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
           else
           xx_log('Process complete =False');
           v_child_approver := l_tab_approvers(1).name;      
           wf_engine.threshold :=1.5 ;--- -1;
           v_child_itemkey := itemkey||'-'||l_tab_approvers(1).approver_order_number;
           v_child_userkey := 'LOT-'||v_child_itemkey;
           v_child_approver := l_tab_approvers(1).name;
           ---
           wf_engine.createprocess(itemtype=>itemtype,
           itemkey=>v_child_itemkey,
           process=>'APLOTAPR_DETAIL'
           );
           wf_engine.setitemuserkey(itemtype, v_child_itemkey, v_child_userkey);
           wf_engine.setitemowner(itemtype, v_child_itemkey, v_child_approver);

            wf_engine.setitemattrtext(itemtype, v_child_itemkey, 'APPROVER',v_child_approver);
            wf_engine.setitemattrtext(itemtype, v_child_itemkey, 'MASTER_ITEM_KEY', itemkey);
           
          wf_engine.startprocess(itemtype, v_child_itemkey);
           end if;
    v_step :='X9';
    exception when others then xx_log('is_approval_complete :'||SQLERRM);
     xx_log('v_step :'||v_step);
    end is_approval_complete_old;
    --
     PROCEDURE is_approval_complete (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    ) is 
    l_approvalProcessCompleteYNOut varchar2(1):=null;
     l_tab_approvers ame_util.approversTable2;
     v_step varchar2(30);
     l_next_approver ame_util.approverRecord;
     v_child_itemkey varchar2(60);
     v_child_approver  varchar2(60);
     v_child_userkey varchar2(60);
     l_LOT_HEADER_ID varchar2(60);
    begin
    v_step :='X0';
 
  l_LOT_HEADER_ID :=wf_engine.getitemattrtext(itemtype, itemkey, 'LOT_HEADER_ID');
  
        AME_API2.getNextApprovers4(applicationIdIn =>200,
                              transactionTypeIn =>'APINVLOT',
                              transactionIdIn =>l_LOT_HEADER_ID ,-- '26',
                              flagApproversAsNotifiedIn => ame_util.booleanFalse,
                              approvalProcessCompleteYNOut => l_approvalProcessCompleteYNOut,
                              nextApproversOut =>l_tab_approvers);
    v_step :='X1';
    if (l_tab_approvers.count = 0 ) then
    xx_log('is_approval_complete :: l_tab_approvers.count = 0  ');
               resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
    return;
    end if;
    v_step :='X1.1';

 
             v_step :='X2';
            xx_log('is_approval_complete :: l_approvalProcessCompleteYNOut='||l_approvalProcessCompleteYNOut);
         -- xx_log('-->approver name :'||l_next_approver.first_name||', '||l_next_approver.last_name);
           
              v_step :='X3';
           if (l_approvalProcessCompleteYNOut = ame_util.booleanTrue) then
           v_step :='X3.1';
           xx_log('is_approval_complete :: Process complete =True');
           resultout := wf_engine.eng_completed
                     || ':'
                     || 'Y';
           else
           v_step :='X3.2';
            xx_log('is_approval_complete :: approver name :'||l_tab_approvers(1).name);  
           v_child_approver := l_tab_approvers(1).name;
           wf_engine.setitemattrtext(itemtype, itemkey, 'APPROVER',v_child_approver);
                resultout := wf_engine.eng_completed
                     || ':'
                     || 'N';
       
           end if;
    v_step :='X9';
    exception when others then xx_log('is_approval_complete :'||SQLERRM);
     xx_log('v_step :'||v_step);
    end is_approval_complete;
    
    --
    PROCEDURE wait_for_approver_response(
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    )is 
    begin
    null;
    exception when others then xx_log('wait_for_approver_response :'||SQLERRM);
    end;
    
    PROCEDURE get_approver_response(
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    )is 
    v_approver varchar2(60);
    v_result varchar2(60);
    v_masteritemkey  varchar2(60);
    v_approverIn ame_util.approverRecord2;
    v_step varchar2(30);
    v_status varchar2(20) ;
    l_LOT_HEADER_ID varchar2(60); 
    begin
    v_step :='X0';
    xx_log('get_approver_response :: get_approver_response funcmode='||funcmode);
    
    l_LOT_HEADER_ID :=wf_engine.getitemattrtext(itemtype, itemkey, 'LOT_HEADER_ID');
    
    v_approver :=wf_engine.getitemattrtext(
    itemType =>itemtype
    ,itemKey =>itemkey
    ,aname=>'APPROVER'
    );
    
        v_result :=wf_engine.getitemattrtext(
    itemType =>itemtype
    ,itemKey =>itemkey
    ,aname=>'RESULT'
    );
    v_result := nvl(v_result,'@');
     xx_log('get_approver_response :: get_approver_response v_result='||v_result);
 
    
    v_step :='X1'; 
    xx_log('get_approver_response :: v_approver==>'||v_approver);
    v_approverIn.name:=v_approver;
 
 
IF ( v_result in ( 'REJECT','OK') ) THEN
    v_status := ame_util.rejectstatus ;
   
ELSif  ( v_result = 'APPROVE' ) THEN
    v_status :=ame_util.approvedstatus ;
else
   return;
END IF;


 
IF ( v_result in ( 'REJECT','OK') ) THEN
    update_approval_hist(itemtype,itemkey, l_LOT_HEADER_ID,'REJECT');
   
ELSif  ( v_result = 'APPROVE' ) THEN
   update_approval_hist(itemtype,itemkey, l_LOT_HEADER_ID,'APPROVED');
END IF;


ame_api2.updateapprovalstatus2 ( 
applicationidin => 200
, transactionidin =>l_LOT_HEADER_ID --'26'
,approvalStatusIn => v_status--AME_UTIL.approvedStatus --,–approved_status_or_rejected_status 
,approvernamein => v_approver
, transactiontypein => 'APINVLOT' );                                 
xx_log('get_approver_response ::  ame_api2.updateapprovalstatus2 ** APPROVER='||v_approver||'  ---'||v_status||'--'); 
wf_engine.setitemattrtext(
    itemType =>itemtype
    ,itemKey =>itemkey
    ,aname=>'RESULT'
    ,aValue=>null
    );
      v_step :='X2';                           
     v_masteritemkey := wf_engine.getItemAttrText(
     itemtype =>itemtype,
     itemkey=>itemkey,
     aname=> 'MASTER_ITEM_KEY'
     );
     xx_log('get_approver_response :: v_masteritemkey='||v_masteritemkey);
     /*
     v_step :='X3';
     wf_engine.completeActivity(
     itemtype=>itemtype,
     itemkey =>itemkey  ,--v_masteritemkey,
     activity =>'Wait For Approver Response',
     result=>null
     )   ;     */         
     v_step :='X4';
    resultout := wf_engine.eng_completed||':'||'Y';
    exception when others then 
    xx_log('get_approver_response :: get_approver_response :'||SQLERRM);
    xx_log('get_approver_response :: v_step='||v_step);
    end;
-- new

PROCEDURE update_approval_hist (
            itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
    ,p_lot_header_id  NUMBER
  , p_response       VARCHAR2
) IS

    l_hist_rec        ap_inv_aprvl_hist%rowtype;
    CURSOR cur_lot (
        c_lot_header_id NUMBER
    ) IS
    SELECT
        invoice_id
    FROM
        xxsgma_invlot_lines
    WHERE
            lot_header_id = c_lot_header_id
        AND
        selected = 'Y';

    l_iteration       NUMBER;
    l_notf_iteration  NUMBER;
    l_approver_id     NUMBER;
    l_approver_name   VARCHAR2(150);
    l_org_id          NUMBER(15);
    l_invoice_total   NUMBER;
    l_comments        ap_inv_aprvl_hist.approver_comments%TYPE;
BEGIN
  
     l_approver_name := WF_ENGINE.GetItemAttrText(itemtype,                             itemkey,                             'APPROVER');
   --l_document_approver := WF_ENGINE.GetItemAttrText(itemtype,                             itemkey,                             'DOCUMENT_APPROVER');
   --l_approver_id := WF_ENGINE.GETITEMATTRNumber(itemtype,                   itemkey,                   'APPROVER_ID');
    FOR r0 IN cur_lot(p_lot_header_id) LOOP
        l_hist_rec.history_type := 'DOCUMENTAPPROVAL';
        l_hist_rec.invoice_id := r0.invoice_id;
        l_hist_rec.iteration := l_iteration;
        l_hist_rec.notification_order := l_notf_iteration;
        l_hist_rec.response := p_response;
        l_hist_rec.approver_id := l_approver_id;
        l_hist_rec.approver_name := l_approver_name;
        l_hist_rec.created_by := nvl(to_number(fnd_profile.value('USER_ID')), -1);

        l_hist_rec.creation_date := sysdate;
        l_hist_rec.last_update_date := sysdate;
        l_hist_rec.last_updated_by := nvl(to_number(fnd_profile.value('USER_ID')), -1);

        l_hist_rec.last_update_login := nvl(to_number(fnd_profile.value('LOGIN_ID')), -1);

        l_hist_rec.org_id := l_org_id;
        l_hist_rec.amount_approved := l_invoice_total;
        l_hist_rec.approver_comments := 'lot:'||p_lot_header_id ||' * '||l_comments;
         AP_WORKFLOW_PKG.insert_history_table(p_hist_rec => l_hist_rec);
    END LOOP;      end;
END xxsgma_ap_wflot_pkg;