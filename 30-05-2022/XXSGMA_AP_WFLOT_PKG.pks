CREATE OR REPLACE PACKAGE xxsgma_ap_wflot_pkg AUTHID current_user AS
/* $Header:  v1.0 SDINARI : ATOS 2022 (MOROCCO) $ */
--  Public Procedure Specifications


                PROCEDURE process_doc_approval (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE set_document_approver (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE notification_handler (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE reject_handler (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE process_doc_rejection (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE forward_check (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE escalate_doc_approval (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE process_sub_approval_response (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

--new

            PROCEDURE is_approval_complete (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE wait_for_approver_response (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );

    PROCEDURE get_approver_response (
        itemtype   IN  VARCHAR2
      , itemkey    IN  VARCHAR2
      , actid      IN  NUMBER
      , funcmode   IN  VARCHAR2
      , resultout  OUT NOCOPY VARCHAR2
    );
-- new
            PROCEDURE show_lot_detail (
        document_id    IN  VARCHAR2
      , display_type   IN  VARCHAR2
      , document       IN OUT NOCOPY VARCHAR2
      , document_type  IN OUT NOCOPY VARCHAR2
    );

    PROCEDURE start_lot_approval (
        p_lot_header_id NUMBER
    );

    PROCEDURE start_lot_approval2 (
        p_lot_header_id NUMBER
    );

    PROCEDURE get_lot_info (
        p_lot_header_id    IN      NUMBER
      , p_nombre_factures  IN OUT  NUMBER
      , p_total_factures   IN OUT  NUMBER
    );

    PROCEDURE update_approval_hist (
        itemtype         IN  VARCHAR2
      , itemkey          IN  VARCHAR2
      , p_lot_header_id  NUMBER
      , p_response       VARCHAR2
    );

END xxsgma_ap_wflot_pkg;