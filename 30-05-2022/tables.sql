drop table XXSGMA.XXSGMA_INVLOT_HEADERS;
drop table XXSGMA.XXSGMA_INVLOT_LINES;

CREATE TABLE XXSGMA.XXSGMA_INVLOT_HEADERS(
LOT_HEADER_ID   NUMBER,
NUM_LOT         VARCHAR2(40),
TOTAL_MNT_LOT   NUMBER,
NOMBRE_FACTURES NUMBER,
USER_ID         NUMBER,
DATE_LOT        DATE,
STATUT_LOT      VARCHAR2(60),
COMMENTAIRE     VARCHAR2(240)
);

CREATE PUBLIC SYNONYM XXSGMA_INVLOT_HEADERS FOR  XXSGMA.XXSGMA_INVLOT_HEADERS ;

CREATE TABLE XXSGMA.XXSGMA_INVLOT_LINES(
LOT_HEADER_ID   NUMBER,
LOT_LINE_ID     NUMBER,
INVOICE_ID      NUMBER,
SELECTED        VARCHAR2(1)
);

CREATE PUBLIC SYNONYM XXSGMA_INVLOT_LINES FOR XXSGMA.XXSGMA_INVLOT_LINES;

--VIEW
create or replace view XXSGMA_INVLOT_LINES_POP as
select 
APINV.INVOICE_ID
,APINV.INVOICE_NUM AS NUM_FACTURE
,APINV.DESCRIPTION AS DESIGNATION
,nvl(apinv.EXCHANGE_RATE,1)*APINV.INVOICE_AMOUNT AS MNT_TTC
,APINV.INVOICE_CURRENCY_CODE AS DEVISE
,APS.VENDOR_NAME AS FOURNISSEUR
,APSS.VENDOR_SITE_CODE AS SITE_FOURNISSEUR
,APINV.INVOICE_DATE AS DATE_FACTURE
from 
ap_invoices_all  apinv
,ap_suppliers aps
,ap_supplier_sites_all apss
where 
1=1
and apinv.vendor_id=aps.vendor_id
and apinv.vendor_site_id=apss.vendor_site_id
and not exists (
select 1 from 
   XXSGMA_INVLOT_HEADERS loth
  ,XXSGMA_INVLOT_LINES lotl
where 
    loth.lot_header_id=lotl.lot_header_id
and lotl.invoice_id=apinv.invoice_id
and loth.STATUT_LOT in ('Brouillon')
)
and upper(ap_invoices_pkg.GET_WFAPPROVAL_STATUS(apinv.invoice_id,apinv.org_id)) <> upper('WFAPPROVED')
--and rownum < 11
;

--SEQUENCES
create sequence xxsgma_invlot_h;
create sequence xxsgma_invlot_l;