cd /home/oracle
. /u01/install/APPS/EBSapps.env run

FORMS=XXINVLOTAPR
cd $AU_TOP/forms/US

rm $FORMS.fmx 
rm $AP_TOP/forms/US/$FORMS.fmx 

frmcmp_batch userid=apps/apps module_type=FORM compile_all=y module=$FORMS.fmb  output_file=$FORMS.fmx

cp $FORMS.fmx $AP_TOP/forms/US/
